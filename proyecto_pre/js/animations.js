import bodymovin from 'lottie-web';

export function initYearAnimation(obj, initVal, lastVal, duration) {

    let startTime = null;

    //get the current timestamp and assign it to the currentTime variable
    let currentTime = Date.now();

    //pass the current timestamp to the step function
    const step = function(currentTime) {

        //if the start time is null, assign the current time to startTime
        if (!startTime) {
            startTime = currentTime ;
        }

        //calculate the value to be used in calculating the number to be displayed
        const progress = Math.min((currentTime  - startTime) / duration, 1);

        //calculate what to be displayed using the value gotten above
        obj.innerHTML = Math.floor(progress * (lastVal - initVal) + initVal);

        //checking to make sure the counter does not exceed the last value (lastVal)
        if (progress < 1) {
            window.requestAnimationFrame(step);
        } else{
            window.cancelAnimationFrame(window.requestAnimationFrame(step));
        }
    };

    //start animating
    window.requestAnimationFrame(step);
}

export function initJobsIconsAnimation() {
    let icons = document.getElementsByClassName('jobs__icons');
    for ( let i = 0; i < icons.length; i++ ) {
        setTimeout(function(){
            showJobsIcons(icons[i]);
        },i*200);
    }

    function showJobsIcons(icons) {
	    let iconsNumber = icons.getAttribute('data-icons');
	    for ( let i = 0; i < iconsNumber; i++ ) {
	        addJobsIcon(icons,i,iconsNumber);
	    }
	}

	function addJobsIcon(parent,position,limit) {
	    let icon = document.createElement('img');
	    icon.src = "https://www.ecestaticos.com/file/ce078802d4b7dbf03f3ed614e1ad5f95/1623999285-empleos-icon.svg";
	    icon.className = "jobs__icon";
	    parent.appendChild(icon);
	    setTimeout(function(){
	        icon.classList.add("jobs__icon--visible");
	        if ( position == limit - 1 ) {
	            parent.nextElementSibling.classList.add('jobs__number--visible');
	        }
	    },150*position);
	}
}

export function loadBodymovin(index) {
	let viewportWidth = window.innerWidth;
	let container, data, animation, firstFrame;
	if ( index == 0 ) {
		container = document.getElementById('bodymovinFirst');
		data = (window.innerWidth < 993) ? 'https://www.ecestaticos.com/file/0fe8ad48be859000acb33f02b25e85be/1625692938-ilu1_mvl.json' : 'https://www.ecestaticos.com/file/664c9bcace84560dad89e6ffe51cab98/1625485958-ilu1.json';
		//data = 'https://www.ecestaticos.com/file/c29ee3546e50861719cb09573067e964/1625206929-ilu1.json';
		//firstFrame = 144;
		firstFrame = 230;
		/*let videoFirstRatio = 1.03; // = 824px de ancho / 800px de alto
    	let cardRatio = container.parentElement.offsetWidth / container.parentElement.offsetHeight;
    	let bodymovinClass = ( cardRatio > videoFirstRatio ) ? "bodymovin--height" : "bodymovin--width";
    	container.classList.add(bodymovinClass);*/
	} else if ( index == 1 ) {
		container = document.getElementById('bodymovinSecond');
		//data = 'https://www.ecestaticos.com/file/3a82166fd904afaf6dd9d77f662d37b9/1625207118-ilu4.json';
		data = (window.innerWidth < 993) ? 'https://www.ecestaticos.com/file/428019f4d80fb92bffac21e1fe76f177/1625692938-ilu2_mvl.json' : 'https://www.ecestaticos.com/file/0dc28095209282783a4ea112eb81ec3e/1625692938-ilu2.json';
		//firstFrame = 220;
		//firstFrame = 616;
		firstFrame = 1;
	} else if ( index == 2 ) {
		container = document.getElementById('bodymovinThird');
		//data = 'https://www.ecestaticos.com/file/0d14373de837a6184a8b51b625b2cf38/1625207136-ilu2.json';
		data = (window.innerWidth < 993) ? 'https://www.ecestaticos.com/file/240b9abe40f025c24b17856621c62b22/1625692938-ilu3_mvl.json' : 'https://www.ecestaticos.com/file/5b652341268c6e93f0bb4f983e73fd39/1625692938-ilu3.json';
		//firstFrame = 526;
		firstFrame = 220;
	} else if ( index == 3 ) {
		container = document.getElementById('bodymovinFourth');
		//data = 'https://www.ecestaticos.com/file/4f5d44af6852bcfba3e0057a32c1d198/1625206976-ilu3.json';
		data = (window.innerWidth < 993) ? 'https://www.ecestaticos.com/file/d4f8d30cd1fd368ec8ff80291416e04f/1625692938-ilu4_mvl.json' : 'https://www.ecestaticos.com/file/bd74c0bba32a957b3576b61f0ccdb877/1625485958-ilu4.json';
		//firstFrame = 616;
		firstFrame = 526;
	}
	if ( !container.classList.contains('bodymovin--loaded') ) {
		container.classList.add('bodymovin--loaded')
		animation = bodymovin.loadAnimation({
			container: container,
		  	renderer: 'svg',
		  	loop: false,
		  	autoplay: true,
		  	//assetsPath: 'https://www.ecestaticos.com/file/',
		  	path: data
		})
		animation.addEventListener("complete",function(){
			//console.log("He acabado!");
			//console.log("Total frames "+animation.totalFrames);
			animation.goToAndStop(firstFrame, true);
			animation.play();
		})
	}
	/*if ( index == 0 ) {
		let videoFirst = document.getElementById("videoFirst");
	    if ( videoFirst.src === '' ) {
	    	let videoFirstRatio = 1.03; // = 824px de ancho / 800px de alto
	    	let cardRatio = videoFirst.parentElement.offsetWidth / videoFirst.parentElement.offsetHeight;
	    	let bodymovinClass = ( cardRatio > videoFirstRatio ) ? "bodymovin--height" : "bodymovin--width";
	    	videoFirst.classList.add(bodymovinClass);
	        let videoFirstSrc = "https://www.ecestaticos.com/file/72f8d08d0c35456cfa459dd7e7c9c933/1624465283-ilu1.mp4";
	        videoFirst.src = videoFirstSrc;
	    }
	} else if ( index == 1 ) {
		let videoSecond = document.getElementById("videoSecond");
	    if ( videoSecond.src === '' ) {
	    	let videoSecondRatio = 1.03; // = 824px de ancho / 800px de alto
	    	let cardRatio = videoSecond.parentElement.offsetWidth / videoSecond.parentElement.offsetHeight;
	    	let bodymovinClass = ( cardRatio > videoSecondRatio ) ? "bodymovin--height" : "bodymovin--width";
	    	videoSecond.classList.add(bodymovinClass);
	        let videoSecondSrc = "https://www.ecestaticos.com/file/28b16b99927fbcb03161f7498d19b843/1624534645-ilu4.mp4";
	        videoSecond.src = videoSecondSrc;
	    }
	}  else if ( index == 2 ) {
		let videoThird = document.getElementById("videoThird");
	    if ( videoThird.src === '' ) {
	    	let videoThirdRatio = 1.03; // = 824px de ancho / 800px de alto
	    	let cardRatio = videoThird.parentElement.offsetWidth / videoThird.parentElement.offsetHeight;
	    	let bodymovinClass = ( cardRatio > videoThirdRatio ) ? "bodymovin--height" : "bodymovin--width";
	    	videoThird.classList.add(bodymovinClass);
	        let videoThirdSrc = "https://www.ecestaticos.com/file/0e96cd429c2d6c71b1f65a1e2afaccf1/1624483324-ilu2.mp4";
	        videoThird.src = videoThirdSrc;
	    }
	} else if ( index == 3 ) {
		let videoFourth = document.getElementById("videoFourth");
	    if ( videoFourth.src === '' ) {
	    	let videoFourthRatio = 1.03; // = 824px de ancho / 800px de alto
	    	let cardRatio = videoFourth.parentElement.offsetWidth / videoFourth.parentElement.offsetHeight;
	    	let bodymovinClass = ( cardRatio > videoFourthRatio ) ? "bodymovin--height" : "bodymovin--width";
	    	videoFourth.classList.add(bodymovinClass);
	        let videoFourthSrc = "https://www.ecestaticos.com/file/cadbc87f8bba47ba479992be44363c24/1624483603-ilu3.mp4";
	        videoFourth.src = videoFourthSrc;
	    }
	}*/
}