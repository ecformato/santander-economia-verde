export function initGallery() {
	let activeItem = 1;
	let itemsLength = document.getElementsByClassName("gallery__item").length;
	//Arrows navigation
	let arrowLeft = document.getElementById("galleryArrowLeft");
	let arrowRight = document.getElementById("galleryArrowRight");

	let galleryWidth = document.getElementById("galleryLayer").offsetWidth;

	setGalleryItemsWidth();
	setGalleryImagesClass();
	setNavigationListeners();

	function setGalleryItemsWidth() {
		let galleryItems = document.getElementsByClassName("gallery__item");

		for ( let i = 0; i < galleryItems.length; i++ ) {
			galleryItems[i].style.width = `${galleryWidth}px`;
		}
	}

	function setGalleryImagesClass() {
		//let galleryImagesRatio = 1.0325; // = 826px de ancho / 800px de alto
		let galleryImagesRatio = window.innerWidth < 993 ? 0.56646525679 : 1.0325;
		let galleryRatio = document.getElementById("galleryLayer").offsetWidth / document.getElementById("galleryLayer").offsetHeight;
		let galleryImageClass = ( galleryRatio > galleryImagesRatio ) ? "gallery__image--width" : "gallery__image--height";
		let galleryImages = document.getElementsByClassName("gallery__image");

		for ( let i = 0; i < galleryImages.length; i++ ) {
			galleryImages[i].classList.add(galleryImageClass);
		}
	}

	function setNavigationListeners() {
		//Arrow left
		arrowLeft.addEventListener("click", function(e){
			e.preventDefault();
			if ( activeItem > 0 ) {
				activeItem--;
				updateGallery();
			}
		});
		//Arrow right
		arrowRight.addEventListener("click", function(e){
			e.preventDefault();
			if ( activeItem < itemsLength ) {
				activeItem++;
				updateGallery();
			}
		});
	}

	function updateGallery() {
		translateGalleryItems();
		updateArrowsNavigation();
		updateDots();
	}

	function translateGalleryItems() {
		let translation = galleryWidth * (activeItem - 1);
		document.getElementById("galleryItems").style.transform = `translateX(-${translation}px)`;
	}

	function updateArrowsNavigation() {
		//Arrow left
		if ( activeItem == 1 ) {
			arrowLeft.classList.add("gallery__arrow--disabled");
		} else {
			if ( arrowLeft.classList.contains("gallery__arrow--disabled") ) {
				arrowLeft.classList.remove("gallery__arrow--disabled");
			}
		}
		//Arrow right
		if ( activeItem == itemsLength ) {
			arrowRight.classList.add("gallery__arrow--disabled");
		} else {
			if ( arrowRight.classList.contains("gallery__arrow--disabled") ) {
				arrowRight.classList.remove("gallery__arrow--disabled");
			}
		}
	}

	function updateDots() {
		document.getElementsByClassName("gallery__dot--active")[0].classList.remove("gallery__dot--active");
		document.getElementsByClassName("gallery__dot")[activeItem-1].classList.add("gallery__dot--active");
	}
}

export function initGalleryMobile() {
	let activeItem = 1;
	let itemsLength = document.getElementsByClassName("milestone").length;
	//Arrows navigation
	let arrowLeft = document.getElementById("chronologyArrowLeft");
	let arrowRight = document.getElementById("chronologyArrowRight");

	updateChronologyNavigationPosition();
	setScrollableContainers();
	setNavigationListeners();

	function updateChronologyNavigationPosition() {
		let topPosition = document.getElementsByClassName("back")[activeItem-1].getBoundingClientRect().top + 20;
		document.getElementById("chronologyNavigation").style.top = `${topPosition}px`;
	}

	function setScrollableContainers() {
		let elementTotalHeight;
		let containers = document.getElementsByClassName("milestone__content");
		for (let i = 0; i < containers.length; i++) {
			elementTotalHeight = document.getElementsByClassName("chronology")[0].offsetHeight - document.getElementsByClassName("front")[i].offsetHeight - ( 76 * 2 );
			containers[i].style.maxHeight = `${elementTotalHeight}px`;
		}
	}

	function setNavigationListeners() {
		//Arrow left
		arrowLeft.addEventListener("click", function(e){
			e.preventDefault();
			if ( activeItem > 0 ) {
				activeItem--;
				updateGallery();
			}
		});
		//Arrow right
		arrowRight.addEventListener("click", function(e){
			e.preventDefault();
			if ( activeItem < itemsLength ) {
				activeItem++;
				updateGallery();
			}
		});
	}

	function updateGallery() {
		setVisibleItem();
		updateArrowsNavigation();
		updateChronologyNavigationPosition();
	}

	function setVisibleItem() {
		document.getElementsByClassName("milestone--visible")[0].classList.remove("milestone--visible");
		document.getElementsByClassName("milestone")[activeItem-1].classList.add("milestone--visible");
	}

	function updateArrowsNavigation() {
		//Arrow left
		if ( activeItem == 1 ) {
			arrowLeft.classList.add("chronology__arrow--disabled");
		} else {
			if ( arrowLeft.classList.contains("chronology__arrow--disabled") ) {
				arrowLeft.classList.remove("chronology__arrow--disabled");
			}
		}
		//Arrow right
		if ( activeItem == itemsLength ) {
			arrowRight.classList.add("chronology__arrow--disabled");
		} else {
			if ( arrowRight.classList.contains("chronology__arrow--disabled") ) {
				arrowRight.classList.remove("chronology__arrow--disabled");
			}
		}
	}
}