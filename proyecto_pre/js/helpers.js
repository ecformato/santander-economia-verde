import { initCarouselMobile } from './carousel/mobile.js';
import { initCarouselDesktop } from './carousel/desktop.js';

function setHeight() {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
}

function setRRSSLogic() {
	let sharing = document.getElementById('sharing');
	document.getElementById("rrssIcon").addEventListener('click', function(e){
		e.preventDefault();
		if ( sharing.classList.contains('share--visible') ) {
			sharing.classList.remove('share--visible')
		} else {
			sharing.classList.add('share--visible')
		}
	});
}

function setCarouselLogic() {
    //if(window.innerWidth < 768) {
    if(window.innerWidth < 993) {
        initCarouselMobile();
        setListenersAnimations();
    } else {
        initCarouselDesktop();
    }
}

function setListenersAnimations() {
	let textAnimations = [
		"Reduciendo las emisiones de CO2 y la huella de carbono para hacer que nuestro ecosistema sea mucho más sostenible.",
		"Ayudando a reducir los niveles de pobreza y a luchar por la igualdad de todos los colectivos.",
		"Creando un modelo financiero que evite la sobreexplotación de recursos.",
		"Alinear su cartera para cumplir los objetivos de París.",
		"Desarrollar productos verdes para sus clientes.",
		"Reducir la huella ambiental del banco."
	];
	//let animationContent = document.getElementById("animationContent");
	//let animationText = document.getElementById("animationText");
	let animationContent, animationText;
	let buttons = document.getElementsByClassName("animation__more");
	for ( let i = 0; i < buttons.length; i++ ) {
		buttons[i].addEventListener("click", function(e) {
			e.preventDefault();
			animationContent = document.getElementById(buttons[i].getAttribute("data-content"));
			animationText = document.getElementById(buttons[i].getAttribute("data-text"));
			animationText.innerHTML = textAnimations[i];
			animationContent.classList.add("animation__content--visible");
		})
	}
	let animations = document.getElementsByClassName("animation");
	for ( let i = 0; i < animations.length; i++ ) {
		animations[i].addEventListener("click", function(e) {
			if ( !((e.target.classList.contains('animation__text')) || (e.target.classList.contains('animation__more')) || (e.target.classList.contains('animation__arrow'))) ) {
			//if ( !e.target.classList.contains('animation__text') ) {
				//console.log(animations[i].getElementsByClassName("animation__content--visible")[0]);
				//console.log(animations[i].getElementsByClassName("animation__content--visible").length);
				if ( (animations[i].getElementsByClassName("animation__content--visible")[0] !== undefined) && (animations[i].getElementsByClassName("animation__content--visible").length > 0) ) {
					//console.log(animations[i].getElementsByClassName("animation__content--visible")[0]);
					animations[i].getElementsByClassName("animation__content--visible")[0].classList.remove("animation__content--visible");
				}
			}
		})
	}
}

function setScrollableContainer(container) {
	let topbarHeight = document.getElementById("topbar").offsetHeight;
	let navigationHeight = document.getElementById("arrowsNavigation").offsetHeight;
	const margin = 48; //margin-top 24px + 24px margin-bottom
	let baseHeight = (window.innerWidth < 993) ? 260 : topbarHeight + navigationHeight + margin;
	let totalHeight = window.innerHeight - baseHeight;
	let elementTotalHeight;
	
	let contents = document.getElementsByClassName('container')[container].getElementsByClassName("card__content");
	for (let i = 0; i < contents.length; i++) {
		if (typeof contents[i] !== 'undefined') {
			elementTotalHeight = (contents[i].parentElement.previousElementSibling) ? totalHeight - contents[i].parentElement.previousElementSibling.offsetHeight : totalHeight;
			contents[i].style.maxHeight = `${elementTotalHeight}px`;
			if ( hasScroll(contents[i]) ) {
				setBottomGradient(contents[i]);
				addScrollListener(contents[i]);
			}
		}
	}
}

function hasScroll(element) {
	return element.scrollHeight > element.clientHeight;
}

function setBottomGradient(container) {
	container.parentElement.classList.add('gradient-bottom');
}

function addScrollListener(container) {
	container.addEventListener('scroll', function() {
		let scrollTop = this.scrollTop;
		if ( scrollTop > 10 ) {
			container.parentElement.classList.add('gradient-top');
		} else {
			container.parentElement.classList.remove('gradient-top');
		}

		if ( scrollTop > ( container.scrollHeight - container.clientHeight - 10 ) ) {
			container.parentElement.classList.remove('gradient-bottom');
		} else {
			container.parentElement.classList.add('gradient-bottom');
		}
	});
}

function loadVideoHome() {
	let videoHome = document.getElementById("videoHome");
	let videoHomeSrc = ( window.innerWidth < 993 ) ? "https://www.ecestaticos.com/file/5beae8e4f5851357c259e98134c89f79/1624371019-video0_mvl.mp4" : "https://www.ecestaticos.com/file/3953ef89be3e148d92b07048307a1167/1624371594-video0_pc.mp4";
	let videoHomePoster = ( window.innerWidth < 993 ) ? "https://www.ecestaticos.com/file/2ad395d5764f2e164fd982165e075785/1624444812-mvl_portada_video1.jpg" : "https://www.ecestaticos.com/file/eae9fb441aefee6c5bc43e0eea544fa1/1624444366-pc_portada_video1.jpg";
	videoHome.src = videoHomeSrc;
	videoHome.poster = videoHomePoster;
}

export {
    setHeight,
    setRRSSLogic,
    setCarouselLogic,
    setScrollableContainer,
    loadVideoHome
}