import { setScrollableContainer } from '../helpers.js';
import { initGallery } from '../gallery.js';
import { initYearAnimation, initJobsIconsAnimation, loadBodymovin } from '../animations.js';

//Elementos del contenedor superior
let aside = document.getElementById('aside');
let home = document.getElementById('homeBox');
let powered = document.getElementById('powered');
let upperSlidesBlock = document.getElementById('b-slides');
let upperSlidesContainers = document.getElementsByClassName('slide-container');
let upperSlides = document.getElementsByClassName('slide');

//Resto de elementos
let contenedores = document.getElementsByClassName('container');
let contenedorActivo = 0;
let leftArrowContainer = document.getElementsByClassName('arrow--left')[0];
let leftArrowImage = document.getElementById('arrow--left');
let leftArrowText = document.getElementById('arrow--left-text');
let rightArrowContainer = document.getElementsByClassName('arrow--right')[0];
let rightArrowImage = document.getElementById('arrow--right');
let rightArrowText = document.getElementById('arrow--right-text');
let homeIcon = document.getElementById('homeIcon');

//Booleanos para controlar animaciones
let hasJobsIconsAnimationBegun = false; 
let isGalleryInitializated = false;
let hasNavigationBegun = false;

let textArrowDesktopMap = [
    {id: 0, texto: 'PORTADA'},
    {id: 1, texto: 'BLOQUE 1'},
    {id: 2, texto: '1.1 EVOLUCIÓN'},
    {id: 3, texto: '1.2 FRENAR EL CAMBIO CLIMÁTICO'},
    {id: 4, texto: '1.3 MÁS EMPLEO'},
    {id: 5, texto: 'BLOQUE 2'},
    {id: 6, texto: '2.1 ECONOMÍA CIRCULAR'},
    {id: 7, texto: '2.2 ENTREVISTA'},
    {id: 8, texto: 'BLOQUE 3'},
    {id: 9, texto: '3.1 PETICIÓN DE LA ONU'},
    {id: 10, texto: '3.2 BANCO SANTANDER'},
    {id: 11, texto: '3.3 CRONOLOGÍA'},
    {id: 12, texto: 'EC BRANDS'}
];

export function initCarouselDesktop() {
    setTimeout(function(){
        if ( !hasNavigationBegun ) {
            goToNextSlide();
        }
    },25000);


    //Navegación por flechas
    leftArrowContainer.addEventListener('click', function() {
        goToPrevSlide();
    });
    
    rightArrowContainer.addEventListener('click', function() {
        goToNextSlide();
    });

    //Navegación por marcadores superiores
    for(let i = 0; i < upperSlidesContainers.length; i++) {
        upperSlidesContainers[i].addEventListener('click', function(e) {
            //let slideIndex = e.target.children[0].getAttribute('data-slide');
            let slideIndex = this.children[0].getAttribute('data-slide');
            contenedorActivo = +slideIndex;
            showArrows(contenedorActivo);
            setUpperElements(slideIndex);
            setContainer(slideIndex);
            setAnimation(contenedorActivo);
            setScrollableContainer(contenedorActivo);
            setBgVideo(contenedorActivo);
            hasNavigationBegun = true;
        })
    }

    //Botón Home
    homeIcon.addEventListener('click',function(e){
        e.preventDefault();
        let slideIndex = 0;
        contenedorActivo = 0;
        showArrows(contenedorActivo);
        setUpperElements(slideIndex);
        setContainer(slideIndex);
        setAnimation(contenedorActivo);
        setScrollableContainer(contenedorActivo);
        setBgVideo(contenedorActivo);
    });

    //Navegación por teclado
    document.addEventListener('keydown', function(e){
        let code = e.code;
        if ( code == "ArrowLeft" ) {
            goToPrevSlide();
        } else if ( code == "ArrowRight" ) {
            goToNextSlide();
        }
    });

    //Navegación por scroll
    let activeElement;
    let wheelHandler = function(e) {
        let targetElement = e.target;
        //console.log("Entro!");
        //console.log(targetElement);
        if ( targetElement != activeElement ) {
            activeElement = targetElement;
            if ( !targetElement.parentElement.classList.contains("card__content") ) {
                toggleListener(wheelHandler, false)
                //const isScrollingDown = Math.sign(e.wheelDeltaY);
                //let isScrollingDown = Math.sign(e.deltaY);
                //console.log(e);
                if ( (e.deltaY < 0) || (e.deltaX < 0) ) {
                    goToPrevSlide();
                } else if ( (e.deltaY > 0) || (e.deltaX > 0) ) {
                    goToNextSlide();
                }
                setTimeout(function(){
                    toggleListener(wheelHandler, true);
                },800);
            }
        }
    };
    let toggleListener = function(listener, add) {
        if (add) {
            document.addEventListener("wheel", listener)
        } else {
            document.removeEventListener("wheel", listener)
        }
    }
    toggleListener(wheelHandler, true);
}

//Helpers internos
function removeActiveSlide() {
    for (let i = 0; i < contenedores.length; i++) {
        contenedores[i].classList.remove('active');
    }
}

function goToNextSlide() {
    if(contenedorActivo != (contenedores.length - 1) ) {
        removeActiveSlide();
        contenedores[contenedorActivo + 1].classList.add('active');
        contenedorActivo = contenedorActivo + 1;
        showArrows(contenedorActivo);
        setUpperElements();
        setAnimation(contenedorActivo);
        setScrollableContainer(contenedorActivo);
        setBgVideo(contenedorActivo);
        hasNavigationBegun = true;
    }
}

function goToPrevSlide() {
    if (contenedorActivo != 0) {
        for (let i = 0; i < contenedores.length; i++) {
            contenedores[i].classList.remove('active');
        }
        contenedores[contenedorActivo - 1].classList.add('active');
        contenedorActivo = contenedorActivo - 1;
        showArrows(contenedorActivo);
        setUpperElements();
        setAnimation(contenedorActivo);
        setScrollableContainer(contenedorActivo);
        setBgVideo(contenedorActivo);
        hasNavigationBegun = true;
    }
}

function showArrows(idx) {
    //Comprobamos qué tipo de flechas aparecen
    //Desktop
    //CA = 0 > Izq: none; dcha: blanca
    //CA = 1 > Izq y dcha: blanca
    //CA = 2 > Izq: blanca; dcha: negra
    //CA = 3 > Izq: blanca; dcha: negra
    //CA = 4 > Izq: blanca; dcha: negra
    //CA = 5 > Izq y dcha: blanca
    //CA = 6 > Izq: blanca; dcha: negra
    //CA = 7 > Izq y dcha: blanca
    //CA = 8 > Izq y dcha: blanca
    //CA = 9 > Izq: blanca; dcha: negra
    //CA = 10 > Izq: blanca; dcha: negra
    //CA = 11 > Izq y dcha: negra
    //CA = 12 > Izq: negra; dcha: none
    //console.log('idx '+idx);
    if (idx == 0) {
        rightArrowImage.src = 'https://www.ecestaticos.com/file/baabdda833d4e8c592ba6117687930ed/1623055663-flechaderecha_blanca.svg';
    } else if (idx == contenedores.length - 1) {
        leftArrowImage.src = 'https://www.ecestaticos.com/file/7dcad471bbec5045a342c294ac970eca/1623055688-flecha_izq_blanca.svg';
    } else if (idx == 1 || idx == 5 || idx == 7 || idx == 8) {
        leftArrowImage.src = 'https://www.ecestaticos.com/file/7dcad471bbec5045a342c294ac970eca/1623055688-flecha_izq_blanca.svg';
        rightArrowImage.src = 'https://www.ecestaticos.com/file/baabdda833d4e8c592ba6117687930ed/1623055663-flechaderecha_blanca.svg';
    } else if (idx == 11) {
        leftArrowImage.src = 'https://www.ecestaticos.com/file/976edb684dd308badd8fa2242edddf43/1623055691-flecha_izq_negra.svg';
        rightArrowImage.src = 'https://www.ecestaticos.com/file/064c0c4c87fb0d1528ca654690e68fee/1623055666-flechaderecha_negra.svg';
    } else if (idx == 2 || idx == 3 || idx == 4 || idx == 6 || idx == 9 || idx == 10) {
        leftArrowImage.src = 'https://www.ecestaticos.com/file/7dcad471bbec5045a342c294ac970eca/1623055688-flecha_izq_blanca.svg';
        rightArrowImage.src = 'https://www.ecestaticos.com/file/064c0c4c87fb0d1528ca654690e68fee/1623055666-flechaderecha_negra.svg';
    }

    //Comprobamos qué bloque de flechas aparecen
    if(idx == 0) {
        rightArrowContainer.classList.add('arrow--home');
        leftArrowContainer.classList.add('disabled');
    } else {
        //rightArrowContainer.classList.remove('arrow--home');
        leftArrowContainer.classList.remove('disabled');
    }

    if (idx == contenedores.length - 1) {
        rightArrowContainer.classList.add('disabled');
    } else {
        rightArrowContainer.classList.remove('disabled');
    }

    showTextArrows(contenedorActivo); 
}

function showTextArrows(idx) {
    //Añadir una clase css en función del contenedor activo para poner un color u otro
    leftArrowText.classList.remove('black');
    rightArrowText.classList.remove('black');

    //Desktop
    //CA = 0 > Izq: none; dcha: none
    //CA = 1 > Izq y dcha: blanca
    //CA = 2 > Izq: blanca; dcha: negra
    //CA = 3 > Izq: blanca; dcha: negra
    //CA = 4 > Izq: blanca; dcha: negra
    //CA = 5 > Izq y dcha: blanca
    //CA = 6 > Izq: blanca; dcha: negra
    //CA = 7 > Izq y dcha: blanca
    //CA = 8 > Izq y dcha: blanca
    //CA = 9 > Izq: blanca; dcha: negra
    //CA = 10 > Izq: blanca; dcha: negra
    //CA = 11 > Izq y dcha: negra
    //CA = 12 > Izq: negra; dcha: none

    if (idx == contenedores.length - 1) {
        //leftArrowText.classList.add('black');
    } else if (idx == 11) {
        leftArrowText.classList.add('black');
        rightArrowText.classList.add('black');
    } else if (idx == 2 || idx == 3 || idx == 4 || idx == 6 || idx == 9 || idx == 10) {
        rightArrowText.classList.add('black');
    }

    //Comprobar qué textos aparecen
    if(idx == 0) {
        leftArrowText.textContent = '';
        rightArrowText.textContent = 'INICIO';
    } else if (idx == contenedores.length - 1) {
        leftArrowText.textContent = textArrowDesktopMap[idx - 1].texto;
        rightArrowText.textContent = '';
    } else {
        leftArrowText.textContent = textArrowDesktopMap[idx - 1].texto;
        rightArrowText.textContent = textArrowDesktopMap[idx + 1].texto;
    }
}

function setUpperElements(idx = undefined) {
    //VISIBILIZAR ELEMENTOS DEL CONTENEDOR SUPERIOR
    showUpperElements(contenedorActivo);
    showUpperSlides(idx);       
}

function showUpperElements(idx) { //El topbar siempre aparece
    if(idx == 0) {
        //aside.classList.remove('visible');
        homeBox.classList.remove('visible');
        powered.classList.add('visible');
        upperSlidesBlock.classList.remove('visible');
    } else if (idx == contenedores.length - 1) {
        //aside.classList.remove('visible');
        homeBox.classList.add('visible');
        powered.classList.remove('visible');
        upperSlidesBlock.classList.remove('visible');
    } else {
        //aside.classList.add('visible');
        homeBox.classList.remove('visible');
        powered.classList.remove('visible');
        upperSlidesBlock.classList.add('visible');
    }
}

function showUpperSlides(idx = undefined) {
    for(let i = 0; i < upperSlides.length; i++) {
        upperSlides[i].classList.remove('active');
    }

    if(idx) {
        upperSlides[idx].classList.add('active');
    } else {
        upperSlides[contenedorActivo].classList.add('active');
    } 
}

function setContainer(idx) {
    for (let i = 0; i < contenedores.length; i++) {
        contenedores[i].classList.remove('active');
    }

    contenedores[idx].classList.add('active');
}

function setAnimation(idx) {
    //console.log("id "+idx);
    if ( idx == 2) {
        loadBodymovin(0);
    } else if ( idx == 3 ) {
        let year = document.getElementById('cardYear');
        initYearAnimation(year, 2020, 2090, 5000);
        loadBodymovin(1);
    } else if ( idx == 4 ) {
        if ( !hasJobsIconsAnimationBegun ) {
            initJobsIconsAnimation();
            hasJobsIconsAnimationBegun = true;
        }
    } else if ( idx == 6 ) {
        loadBodymovin(2);
    } else if ( idx == 7 ) {
        if ( !isGalleryInitializated ) {
            initGallery();
            isGalleryInitializated = true;
        }
    } else if ( idx == 10 ) {
        loadBodymovin(3);
    }
}

function setBgVideo(idx) {
    //console.log("id "+idx);
    if ( idx == 1 ) {
        let videoBlock1 = document.getElementById("videoBlock1");
        if ( videoBlock1.src === '' ) {
            let videoBlock1Src = "https://www.ecestaticos.com/file/cadac07349ddca79876b9ac900ebb28b/1624372475-video1_pc.mp4";
            let videoBlock1Poster = "https://www.ecestaticos.com/file/c8aa67aadb28a837ff6599f4d0c4870d/1624444923-pc_portada_video2.jpg";
            videoBlock1.src = videoBlock1Src;
            videoBlock1.poster = videoBlock1Poster;
        }
    } else if ( idx == 5 ) {
        let videoBlock2 = document.getElementById("videoBlock2");
        if ( videoBlock2.src === '' ) {
            let videoBlock2Src = "https://www.ecestaticos.com/file/eab5e715fda941b978860b2544d5da0b/1624441837-video2_pc.mp4";
            let videoBlock2Poster = "https://www.ecestaticos.com/file/9d4b5d5dde856698f35a3cc9e7077f7a/1624444977-pc_portada_video3.jpg";
            videoBlock2.src = videoBlock2Src;
            videoBlock2.poster = videoBlock2Poster;
        }
    } else if ( idx == 8 ) {
        let videoBlock3 = document.getElementById("videoBlock3");
        if ( videoBlock3.src === '' ) {
            let videoBlock3Src = "https://www.ecestaticos.com/file/de33d9378c8ba23061690c361d8bfcff/1624441897-video3_pc.mp4";
            let videoBlock3Poster = "https://www.ecestaticos.com/file/64e0e1a033070ff4ac41905f83108432/1624445812-pc_portada_video4.jpg";
            videoBlock3.src = videoBlock3Src;
            videoBlock3.poster = videoBlock3Poster;
        }
    } else if ( idx == 9 ) {
        let videoBlock4 = document.getElementById("videoBlock4");
        if ( videoBlock4.src === '' ) {
            let videoBlock4Src = "https://www.ecestaticos.com/file/de33d9378c8ba23061690c361d8bfcff/1624441897-video3_pc.mp4";
            let videoBlock4Poster = "https://www.ecestaticos.com/file/64e0e1a033070ff4ac41905f83108432/1624445812-pc_portada_video4.jpg";
            videoBlock4.src = videoBlock4Src;
            videoBlock4.poster = videoBlock4Poster;
        }
    } else if ( idx == 12 ) {
        let videoBlock5 = document.getElementById("videoBlock5");
        if ( videoBlock5.src === '' ) {
            let videoBlock5Src = "https://www.ecestaticos.com/file/3953ef89be3e148d92b07048307a1167/1624371594-video0_pc.mp4";
            let videoBlock5Poster = "https://www.ecestaticos.com/file/eae9fb441aefee6c5bc43e0eea544fa1/1624444366-pc_portada_video1.jpg";
            videoBlock5.src = videoBlock5Src;
            videoBlock5.poster = videoBlock5Poster;
        }
    }
}