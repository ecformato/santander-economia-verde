import { setScrollableContainer } from '../helpers.js';
import { initGallery, initGalleryMobile } from '../gallery.js';
import { initYearAnimation, initJobsIconsAnimation, loadBodymovin } from '../animations.js';

//Elementos del contenedor superior
let topbar = document.getElementById('topbar');
let aside = document.getElementById('aside');
let home = document.getElementById('homeBox');
let powered = document.getElementById('powered');
let upperSlidesBlock = document.getElementById('b-slides-mobile');
let upperSlides = document.getElementsByClassName('slide-mobile');

//Otros elementos
let contenedores = document.getElementsByClassName('container');
let cardsMobile = document.getElementsByClassName('card--mobile');
let contenedorActivo = 0;
let cardActivaMobile = 0;
let arrowsNavigation = document.getElementById('arrowsNavigation');
let leftArrowContainer = document.getElementsByClassName('arrow--left')[0];
let leftArrow = document.getElementById('arrow--left');
let leftArrowText = document.getElementById('arrow--left-text');
let rightArrowContainer = document.getElementsByClassName('arrow--right')[0];
let rightArrow = document.getElementById('arrow--right');
let rightArrowText = document.getElementById('arrow--right-text');
let homeIcon = document.getElementById('homeIcon');

//Booleanos para controlar animaciones
let hasJobsIconsAnimationBegun = false;
let isGalleryInitializated = false;
let isGalleryMobileInitializated = false;

let textArrowMobileMap = [
    {id: 0, texto: 'PORTADA'},
    {id: 1, texto: 'BLOQUE 1'},
    //{id: 2, texto: 'BLOQUE 1 (II)'},
    {id: 2, texto: '1.1 EVOLUCIÓN'},
    {id: 3, texto: '1.1 EVOLUCIÓN (II)'},
    {id: 4, texto: '1.2 CAMBIO CLIMÁTICO'},
    {id: 5, texto: '1.2 CAMBIO CLIMÁTICO (II)'},
    {id: 6, texto: '1.3 MÁS EMPLEO'},
    {id: 7, texto: '1.3 MÁS EMPLEO (II)'},
    {id: 8, texto: 'BLOQUE 2'},
    //{id: 10, texto: 'BLOQUE 2 (II)'},
    {id: 9, texto: '2.1 ECONOMÍA CIRCULAR'},
    {id: 10, texto: '2.1 ECONOMÍA CIRCULAR (II)'},
    {id: 11, texto: '2.2 ENTREVISTA'},
    {id: 12, texto: '2.2 ENTREVISTA (II)'},
    {id: 13, texto: 'BLOQUE 3'},
    //{id: 16, texto: 'BLOQUE 3 (II)'},
    {id: 14, texto: '3.1 PETICIÓN A LA ONU'},
    //{id: 15, texto: '3.1 PETICIÓN A LA ONU (II)'},
    {id: 15, texto: '3.2 BANCO SANTANDER'},
    {id: 16, texto: '3.2 BANCO SANTANDER (II)'},
    {id: 17, texto: '3.3 CRONOLOGÍA'},
    {id: 18, texto: 'EC BRANDS'}
];

export function initCarouselMobile() {
    leftArrowContainer.addEventListener('click', function() {
        for (let i = 0; i < cardsMobile.length; i++) {
            cardsMobile[i].classList.remove('active');
        }

        if(cardActivaMobile != 0) {
            cardsMobile[cardActivaMobile - 1].classList.add('active');
            cardActivaMobile = cardActivaMobile - 1;

            //Lógica para mostrar contenedores
            for (let i = 0; i < contenedores.length; i++) {
                contenedores[i].classList.remove('active');
            }

            let auxContainer = cardsMobile[cardActivaMobile].getAttribute('data-container');
            contenedores[auxContainer].classList.add('active');
            contenedorActivo = auxContainer;
        }
    
        showArrows(cardActivaMobile);
        setUpperElements();
        setAnimation(cardActivaMobile);
        setScrollableContainer(contenedorActivo);
        setBgVideo(cardActivaMobile);
    });
    
    rightArrowContainer.addEventListener('click', function() {
        for (let i = 0; i < cardsMobile.length; i++) {
            cardsMobile[i].classList.remove('active');
        }

        if(cardActivaMobile != cardsMobile.length - 1) {
            cardsMobile[cardActivaMobile + 1].classList.add('active');
            cardActivaMobile = cardActivaMobile + 1;

            //Lógica para mostrar contenedores
            for (let i = 0; i < contenedores.length; i++) {
                contenedores[i].classList.remove('active');
            }

            let auxContainer = cardsMobile[cardActivaMobile].getAttribute('data-container');
            contenedores[auxContainer].classList.add('active');
            contenedorActivo = auxContainer;
        }
    
        showArrows(cardActivaMobile);
        setUpperElements();
        setAnimation(cardActivaMobile);
        setScrollableContainer(contenedorActivo);
        setBgVideo(cardActivaMobile);
    });

    //Botón Home
    homeIcon.addEventListener('click',function(e){
        e.preventDefault();
        cardActivaMobile = 0;
        contenedorActivo = 0;
        for (let i = 0; i < cardsMobile.length; i++) {
            cardsMobile[i].classList.remove('active');
        }
        cardsMobile[cardActivaMobile].classList.add('active');
        for (let i = 0; i < contenedores.length; i++) {
            contenedores[i].classList.remove('active');
        }
        contenedores[contenedorActivo].classList.add('active');
        showArrows(cardActivaMobile);
        setUpperElements();
        setAnimation(cardActivaMobile);
        setScrollableContainer(contenedorActivo);
        setBgVideo(cardActivaMobile);
    });

    document.addEventListener('swiped-left', function() {
        if (cardActivaMobile != cardsMobile.length - 1) {
            for (let i = 0; i < cardsMobile.length; i++) {
                cardsMobile[i].classList.remove('active');
            }

            if(cardActivaMobile != cardsMobile.length - 1) {
                cardsMobile[cardActivaMobile + 1].classList.add('active');
                cardActivaMobile = cardActivaMobile + 1;

                //Lógica para mostrar contenedores
                for (let i = 0; i < contenedores.length; i++) {
                    contenedores[i].classList.remove('active');
                }

                let auxContainer = cardsMobile[cardActivaMobile].getAttribute('data-container');
                contenedores[auxContainer].classList.add('active');
                contenedorActivo = auxContainer;
            }
        
            showArrows(cardActivaMobile);
            setUpperElements();
            setAnimation(cardActivaMobile);
            setScrollableContainer(contenedorActivo);
            setBgVideo(cardActivaMobile);
        }
    });
    document.addEventListener('swiped-right', function() {
        if (cardActivaMobile != 0) {
            for (let i = 0; i < cardsMobile.length; i++) {
                cardsMobile[i].classList.remove('active');
            }

            if(cardActivaMobile != 0) {
                cardsMobile[cardActivaMobile - 1].classList.add('active');
                cardActivaMobile = cardActivaMobile - 1;

                //Lógica para mostrar contenedores
                for (let i = 0; i < contenedores.length; i++) {
                    contenedores[i].classList.remove('active');
                }

                let auxContainer = cardsMobile[cardActivaMobile].getAttribute('data-container');
                contenedores[auxContainer].classList.add('active');
                contenedorActivo = auxContainer;
            }
        
            showArrows(cardActivaMobile);
            setUpperElements();
            setAnimation(cardActivaMobile);
            setScrollableContainer(contenedorActivo);
            setBgVideo(cardActivaMobile);
        }
    });
}

//Helpers internos
function showArrows(idx) {
    if (window.innerWidth > 767) {
        if (idx == 0) {
            rightArrow.src = 'https://www.ecestaticos.com/file/baabdda833d4e8c592ba6117687930ed/1623055663-flechaderecha_blanca.svg';
        } else if (idx == contenedores.length - 1) {
            leftArrow.src = 'https://www.ecestaticos.com/file/7dcad471bbec5045a342c294ac970eca/1623055688-flecha_izq_blanca.svg';
        } else if (idx == 1 || idx == 4 || idx == 6 || idx == 8 || idx == 11 || idx == 15) {
            leftArrow.src = 'https://www.ecestaticos.com/file/7dcad471bbec5045a342c294ac970eca/1623055688-flecha_izq_blanca.svg';
            rightArrow.src = 'https://www.ecestaticos.com/file/baabdda833d4e8c592ba6117687930ed/1623055663-flechaderecha_blanca.svg';
        } else if (idx == 3 || idx == 5 || idx == 7 || idx == 10 || idx == 14 || idx == 16) {
            leftArrow.src = 'https://www.ecestaticos.com/file/976edb684dd308badd8fa2242edddf43/1623055691-flecha_izq_negra.svg';
            rightArrow.src = 'https://www.ecestaticos.com/file/064c0c4c87fb0d1528ca654690e68fee/1623055666-flechaderecha_negra.svg';
        }
    } else {
        if (idx == 0) {
            rightArrow.src = 'https://www.ecestaticos.com/file/baabdda833d4e8c592ba6117687930ed/1623055663-flechaderecha_blanca.svg';
        } else {
            leftArrow.src = 'https://www.ecestaticos.com/file/30cc377f47c14794ad81e5f58636303d/1624447874-mvl_flechaizq.svg';
            rightArrow.src = 'https://www.ecestaticos.com/file/03b4047222f7c0f492d35e8fca0e34a5/1624447895-mvl_flechader.svg';
        }
    }

    if(cardActivaMobile == 0) {
        arrowsNavigation.classList.remove('b-arrows--progress');
        rightArrowContainer.classList.add('arrow--home');
        leftArrowContainer.classList.add('disabled');
    } else {
        arrowsNavigation.classList.add('b-arrows--progress');
        rightArrowContainer.classList.remove('arrow--home');
        leftArrowContainer.classList.remove('disabled');
    }

    if (cardActivaMobile == cardsMobile.length - 1) {
        rightArrowContainer.classList.add('disabled');
    } else {
        rightArrowContainer.classList.remove('disabled');
    }

    showTextArrows(cardActivaMobile);    
}

function showTextArrows(idx) {
    if (window.innerWidth > 767) {
        //Añadir una clase css en función del contenedor activo para poner un color u otro
        leftArrowText.classList.remove('black');
        rightArrowText.classList.remove('black');

        if (idx == contenedores.length - 1) {
            //leftArrowText.classList.add('black');
        } else if (idx == 3 || idx == 5 || idx == 7 || idx == 10 || idx == 14 || idx == 16 || idx == 17) {
            leftArrowText.classList.add('black');
            rightArrowText.classList.add('black');
        }

        if(idx == 0) {
            leftArrowText.textContent = '';
            rightArrowText.textContent = '';
            //rightArrowText.textContent = textArrowMobileMap[idx + 1].texto;
        } else if (idx == cardsMobile.length - 1) {
            leftArrowText.textContent = textArrowMobileMap[idx - 1].texto;
            rightArrowText.textContent = '';
        } else {
            leftArrowText.textContent = textArrowMobileMap[idx - 1].texto;
            rightArrowText.textContent = textArrowMobileMap[idx + 1].texto;
        }
    } else {
        if(idx == 0) {
            leftArrowText.textContent = '';
            rightArrowText.textContent = '';
            //rightArrowText.textContent = textArrowMobileMap[idx + 1].texto;
        } else {
            leftArrowText.textContent = 'Anterior';
            rightArrowText.textContent = 'Siguiente';
        }
    }
}

function setUpperElements(idx = undefined) {
    //VISIBILIZAR ELEMENTOS DEL CONTENEDOR SUPERIOR
    showUpperElements(contenedorActivo);
    showUpperSlides(idx);       
}

function showUpperElements(idx) {
    //console.log(idx);
    //console.log(contenedores.length - 1);
    if(idx == 0) {
        topbar.classList.add('visible');
        aside.classList.add('visible');
        homeBox.classList.remove('visible');
        powered.classList.add('visible');
        upperSlidesBlock.classList.remove('visible');
    } else if (idx == contenedores.length - 1) {
        topbar.classList.add('visible');
        aside.classList.add('visible');
        homeBox.classList.add('visible');
        powered.classList.remove('visible');
        upperSlidesBlock.classList.remove('visible');
    } else {
        topbar.classList.remove('visible');
        aside.classList.remove('visible');
        homeBox.classList.remove('visible');
        powered.classList.remove('visible');
        upperSlidesBlock.classList.add('visible');
    }
}

function showUpperSlides() {
    for(let i = 0; i < upperSlides.length; i++) {
        upperSlides[i].classList.remove('active');
    }

    //Bolas
    upperSlides[+contenedorActivo].classList.add('active');
    //Línea
    let lineSlide = document.getElementsByClassName('line-slide-js')[0];
    if(contenedorActivo != 0) {
        lineSlide.style.width = upperSlides[+contenedorActivo].getBoundingClientRect().x - 12.5 + 'px';
    } else {
        lineSlide.style.width = '0px';
    }
    
    //console.log(cardActivaMobile);
    if ( (cardActivaMobile == 3) || (cardActivaMobile == 5) || (cardActivaMobile == 7) || (cardActivaMobile == 10) || (cardActivaMobile == 14) || (cardActivaMobile == 16) ) {
        upperSlidesBlock.classList.add('b-slides-mobile-color');
    } else {
        upperSlidesBlock.classList.remove('b-slides-mobile-color');
    }
}


function setAnimation(idx) {
    //console.log("id "+idx);
    if ( idx == 3 ) {
        loadBodymovin(0);
    } else if ( idx == 4 ) {
        let year = document.getElementById('cardYear');
        initYearAnimation(year,2020,2090,5000);
    } else if ( idx == 5 ) {
        loadBodymovin(1);
    } else if ( idx == 7 ) {
        if ( !hasJobsIconsAnimationBegun ) {
            initJobsIconsAnimation();
            hasJobsIconsAnimationBegun = true;
        }
    } else if ( idx == 10 ) {
        loadBodymovin(2);
    } else if ( idx == 12 ) {
        if ( !isGalleryInitializated ) {
            initGallery();
            isGalleryInitializated = true;
        }
    } else if ( idx == 16 ) {
        loadBodymovin(3);
    } else if ( idx == 17 ) {
        if ( !isGalleryMobileInitializated ) {
            initGalleryMobile();
            isGalleryMobileInitializated = true;
        }
    }
}

function setBgVideo(idx) {
    //console.log("id "+idx);
    if ( idx == 1 ) {
        let videoBlock1Mobile = document.getElementById("videoBlock1Mobile");
        if ( videoBlock1Mobile.src === '' ) {
            let videoBlock1MobileSrc = "https://www.ecestaticos.com/file/6a2cff53040f9dc5110c8ec33e946ec3/1624377201-video1_mvl.mp4";
            let videoBlock1MobilePoster = "https://www.ecestaticos.com/file/0c32f2ec5581849be042fb63046c3c1c/1624446022-mvl_portada_video2.jpg";
            videoBlock1Mobile.src = videoBlock1MobileSrc;
            videoBlock1Mobile.poster = videoBlock1MobilePoster;
        }
    } else if ( idx == 8 ) {
        let videoBlock2Mobile = document.getElementById("videoBlock2Mobile");
        if ( videoBlock2Mobile.src === '' ) {
            let videoBlock2MobileSrc = "https://www.ecestaticos.com/file/b214bd89d91d076efd02173886776c00/1624377451-video2_mvl.mp4";
            let videoBlock2MobilePoster = "https://www.ecestaticos.com/file/46db3782056c67225060ddd875958146/1624446084-mvl_portada_video3.jpg";
            videoBlock2Mobile.src = videoBlock2MobileSrc;
            videoBlock2Mobile.poster = videoBlock2MobilePoster;
        }
    } else if ( idx == 13 ) {
        let videoBlock3Mobile = document.getElementById("videoBlock3Mobile");
        if ( videoBlock3Mobile.src === '' ) {
            let videoBlock3MobileSrc = "https://www.ecestaticos.com/file/f81a8137e2d12ce950ca1948f9c1a1b7/1624377563-video3_mvl.mp4";
            let videoBlock3MobilePoster = "https://www.ecestaticos.com/file/9953896861a5d4b0336e6a36662008ce/1624446145-mvl_portada_video4.jpg";
            videoBlock3Mobile.src = videoBlock3MobileSrc;
            videoBlock3Mobile.poster = videoBlock3MobilePoster;
        }
    } else if ( idx == 18 ) {
        let videoBlock5 = document.getElementById("videoBlock5");
        if ( videoBlock5.src === '' ) {
            let videoBlock5Src = "https://www.ecestaticos.com/file/5beae8e4f5851357c259e98134c89f79/1624371019-video0_mvl.mp4";
            let videoBlock5Poster = "https://www.ecestaticos.com/file/2ad395d5764f2e164fd982165e075785/1624444812-mvl_portada_video1.jpg";
            videoBlock5.src = videoBlock5Src;
            videoBlock5.poster = videoBlock5Poster;
        }
    }
}

/*!
 * swiped-events.js - v1.1.0
 * Pure JavaScript swipe events
 * https://github.com/john-doherty/swiped-events
 * @inspiration https://stackoverflow.com/questions/16348031/disable-scrolling-when-touch-moving-certain-element
 * @author John Doherty <www.johndoherty.info>
 * @license MIT
 */
!function(t,e){"use strict";"function"!=typeof t.CustomEvent&&(t.CustomEvent=function(t,n){n=n||{bubbles:!1,cancelable:!1,detail:void 0};var u=e.createEvent("CustomEvent");return u.initCustomEvent(t,n.bubbles,n.cancelable,n.detail),u},t.CustomEvent.prototype=t.Event.prototype),e.addEventListener("touchstart",function(t){if("true"===t.target.getAttribute("data-swipe-ignore"))return;o=t.target,l=Date.now(),n=t.touches[0].clientX,u=t.touches[0].clientY,a=0,i=0},!1),e.addEventListener("touchmove",function(t){if(!n||!u)return;var e=t.touches[0].clientX,l=t.touches[0].clientY;a=n-e,i=u-l},!1),e.addEventListener("touchend",function(t){if(o!==t.target)return;var e=parseInt(o.getAttribute("data-swipe-threshold")||"20",10),s=parseInt(o.getAttribute("data-swipe-timeout")||"500",10),r=Date.now()-l,c="";Math.abs(a)>Math.abs(i)?Math.abs(a)>e&&r<s&&(c=a>0?"swiped-left":"swiped-right"):Math.abs(i)>e&&r<s&&(c=i>0?"swiped-up":"swiped-down");""!==c&&o.dispatchEvent(new CustomEvent(c,{bubbles:!0,cancelable:!0}));n=null,u=null,l=null},!1);var n=null,u=null,a=null,i=null,l=null,o=null}(window,document);