import { setRRSSLogic, setCarouselLogic, setHeight, loadVideoHome } from './helpers.js';
import { setRRSSLinks } from '../../common_projects_utilities/js/pure-branded-helpers';

//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

//Seteo inicial de lógicas de:
// - RRSS (configuración enlace a compartir en RRSS y gestión de visibilidad de iconos sociales)
// - Carrusel (eventos asociados a clics, transiciones, etc)
// - Alturas (ajuste de anchos y altos en función del dispositivo)
setRRSSLinks();
setHeight();
setRRSSLogic();
setCarouselLogic();
loadVideoHome();
window.addEventListener('resize', () => {
    setHeight();
});